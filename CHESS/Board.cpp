#include "Board.h"
#include <iostream>
Board::Board()
{
    const std::string BOARD_STR = "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr";//string for the build of the board
    int i = 0;
    for (i=0;i< BOARD_STR.size();i++)
    {
        switch (BOARD_STR[i])//build the pieces of the board
        {
        case 'R':
        {

            board[i % SIZE_BOARD][i /SIZE_BOARD] = new Rook('R', WHITE, i/SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'r':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Rook('r', BLACK, i /SIZE_BOARD , i %SIZE_BOARD);
            break;
        }
        case 'k':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new King('k', BLACK, i /SIZE_BOARD, i %SIZE_BOARD);
            break;
        }
        case 'K':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new King('K', WHITE, i /SIZE_BOARD, i %SIZE_BOARD);
            break;
        }
        case 'p':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Pawn('p', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'P':
        {
            board[i %SIZE_BOARD][i / SIZE_BOARD] = new Pawn('P', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'b':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Bishop('b', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'B':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Bishop('B', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'n':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Knight('n', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'N':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Knight('N', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'q':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Queen('q', BLACK, i /SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'Q':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Queen('Q', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }

        default:
            board[i % SIZE_BOARD][i / SIZE_BOARD] = nullptr;
            break;
        }
    }
    this->isWhiteTurn = true;
    
}
void Board::decrypted(std::string strLocation, int* location)//make the move from x and y location to the cross of the board exemple : e2
{
    const char GET_THE_LOCTION_LETTER = 'a', GET_LOCTION_NUM = '8';
    location[Y_PLACE] = strLocation[Y_PLACE] - GET_THE_LOCTION_LETTER;
    location[X_PLACE] = std::abs(strLocation[X_PLACE] - GET_LOCTION_NUM);
}

std::string Board::moveBoard(std::string message)//move the piece in the board
 {
        int source[2] = { 0 };
        std::string location = "";
        Piece* piece = nullptr;

        location += message[0];
        location += message[1];
        decrypted(location, source);

        // Check if indices are within the valid range
        if (source[Y_PLACE] >= MIN && source[Y_PLACE] < SIZE_BOARD && source[X_PLACE] >= MIN && source[X_PLACE] < SIZE_BOARD) 
        {
            piece = this->board[source[Y_PLACE]][source[X_PLACE]];
            location.clear();
            location += message[2];
            location += message[3];
            decrypted(location, source);
            return move(piece, source[X_PLACE], source[Y_PLACE]);
        }
        else {
            // Handle invalid indices
            return "5";
        }
    }



std::string Board::move(Piece* piece,int xLoction,int yLoction)//check with the choosen piece if the move is valid to him
{
    int res = 0,opType=0;
    res = canMove(xLoction, yLoction, piece);    // Check if the piece can move to the given location


    if (res == MOVE_VALID)
    {
        if (piece->getType() == WHITE)//check for what team the piece belong to
        {
            opType = BLACK;
        }
        else
        {
            opType = WHITE;
        }
        if (this->board[yLoction][xLoction] != NULL && (char)tolower(this->board[yLoction][xLoction]->getName()) == 'k')
        {
            eatPiece(xLoction, yLoction);
        }
        // Move the piece to the new location
        this->board[piece->get_yLocation()][piece->get_xLocation()] = nullptr;
        this->board[yLoction][xLoction] = piece;
        piece->set_xLocation(xLoction);
        piece->set_yLocation(yLoction);
        if (isKingChecked(opType, piece->getType()))//check if from the given move the other player king got checked
        {
            res = 1;
        }
        this->isWhiteTurn = !this->isWhiteTurn;
    }
    return to_string(res);
}

int Board::eatPiece(int xPosition, int yPosition) // This function removes a piece from the board
{
    if (xPosition >= MIN && xPosition < SIZE_BOARD && yPosition >= MIN && yPosition < SIZE_BOARD)    // Check if the position is within the board
    {
        Piece* pieceToBeEaten = this->board[yPosition][xPosition];        // Get the piece at the given position
        if (pieceToBeEaten != nullptr)         // If there is a piece at the position
        {
            delete pieceToBeEaten;            // Delete the piece

            this->board[yPosition][xPosition] = nullptr;            // Set the position on the board to null

            return MOVE_VALID;//send that the move is valid
        }
        else
        {
            return MOVE_INVALID; //return an error
        }
    }
    else {
        return 5;//return an error
    }
}
// This function checks if a piece can move to a given position on the board
int Board::canMove(int xPosition, int yPosition, Piece* piece)
{
    int opType = 0, type = 0, res = 0;
    // Check if the piece is not null
    if (piece != nullptr)
    {
        // Get the type of the piece
        type = piece->getType();
        // Determine the type of the opponent
        if (type == WHITE)
        {
            opType = 1;
        }
        else
        {
            opType = 0;
        }
        // Check if it's the white's turn
        if (this->isWhiteTurn)
        {
            // If the piece is not white, return 2
            if (type != WHITE)
            {
                return WRONG_TURN;
            }
        }
        else
        {
            // If the piece is white, return 2
            if (type == WHITE)
            {
                return WRONG_TURN;
            }
        }
        // If there is a piece at the destination
        if (this->board[yPosition][xPosition] != nullptr)
        {
            // If the piece at the destination is of the same type, return 3
            if (this->board[yPosition][xPosition]->getType() == type)
            {
                return SAME_TYPE_PIECE;
            }
        }
        // If the king of the same type is checked, return 4
        if (isKingChecked(type, opType))
        {
            return KING_CHECKED;
        }
        // If the piece is already at the destination, return 7
        if (this->board[yPosition][xPosition] == piece)
        {
            return PIECE_ALREADY_AT_DEST;
        }
        // Check if the move is legitimate for the piece
        res = piece->isLegitMove(xPosition, yPosition, this->board);
        // Return the result
        return res;
    }
    // If the piece is null, return 
    return PIECE_NULL;
}

// This function checks if the king of a given type is checked by any of the opponent's pieces
bool Board::isKingChecked(int typeKing,int typeSoldiers)
{
    int i = 0, j = 0,res=0;
    bool found = false; // Initialize found to false
    int* kingLocation = nullptr; // Initialize the king's location to null
    kingLocation = getKingLocation(typeKing); // Get the king's location

    // Iterate over the board
    for (i = 0; i < SIZE_BOARD && !found; i++)
    {
        for (j = 0; j < SIZE_BOARD && !found; j++)
        {
            // If there is a piece at the current position
            if (this->board[j][i] != nullptr)
            {
                // If the piece is of the type of the soldiers
                if (this->board[j][i]->getType() == typeSoldiers)
                {
                    // Check if the piece can move to the king's location
                    res = canMove(kingLocation[1], kingLocation[0], this->board[j][i]);
                    // If the piece can move to the king's location, set found to true
                    if (res == 0)
                    {
                        found = true;
                    }
                }
            }
        }
    }
    // Return whether the king is checked
    return found;
}

void Board::changeTurn()
{
    this->isWhiteTurn = false;
}
// This function gets the location of the king of a given type
int* Board::getKingLocation(int type)
{
    int i = 0, j = 0;
    int location[2] = { 0 }; // Initialize the location to 0

    // Iterate over the board
    for (i = 0; i < SIZE_BOARD; i++)
    {
        for (j = 0; j < SIZE_BOARD; j++)
        {
            // If there is a piece at the current position
            if (this->board[j][i] != nullptr)
            {
                // If the piece is a king of the given type
                if (this->board[j][i]->getType() == type && (char)tolower(this->board[j][i]->getName()) == 'k')
                {
                    // Get the location of the king
                    location[0] = this->board[j][i]->get_yLocation();
                    location[1] = this->board[j][i]->get_xLocation();
                }
            }
        }
    }

    // Return the location of the king
    return location;
}

