#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "Rook.h"
class Queen :public Piece
{
public:
	Queen(char name, int type, int xLocation, int yLocation);
	~Queen();
	//check if the the queen can reach the choosen block with his way of moving
	int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])override;
};

