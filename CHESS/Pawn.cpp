#include "Pawn.h"

#define MOVE2SQUARE 2

// Constructor for the Pawn class
Pawn::Pawn(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
	this->FirstMove = true; // Initialize FirstMove to true
}

Pawn::~Pawn()
{
	name = ' ';
	type = 0;
	xLocation = 0;
	yLocation = 0;
}

// This function checks if the pawn can move to a given position
int Pawn::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
	int parse = 0, incOrDec = 0;
	// If the destination square is empty
	if (board[yDestPosition][xDestPosition] == nullptr)
	{
		// If the pawn is not moving vertically, return 6
		if (yDestPosition != this->yLocation)
		{
			return MOVE_INVALID;
		}
	}
	else // If the destination square is not empty
	{
		// If the pawn is not moving diagonally, return 6
		if (std::abs(xDestPosition - this->xLocation) != 1 && std::abs(yDestPosition - this->yLocation) != 1)
		{
			return MOVE_INVALID;
		}
	}
	// Calculate the number of squares the pawn is moving forward
	parse = this->type == 0 ? xDestPosition - this->xLocation : this->xLocation - xDestPosition;
	// Determine the direction of movement
	incOrDec = this->type == 0 ? 1 : -1;
	// If the pawn is not moving forward, return 6
	if (parse <= 0)
	{
		return MOVE_INVALID;
	}
	// If the square in front of the pawn is not empty, return 6
	if (board[this->yLocation][this->xLocation + incOrDec] = nullptr)
	{
		return MOVE_INVALID;
	}
	// If it's the pawn's first move
	if (this->FirstMove)
	{
		// If the pawn is moving more than two squares forward, return 6
		if (parse > MOVE2SQUARE)
		{
			return MOVE_INVALID;
		}
		// If the square two squares in front of the pawn is not empty and the pawn is moving two squares forward, return 6
		if (board[this->yLocation][this->xLocation + (incOrDec * MOVE2SQUARE)] != nullptr && parse == MOVE2SQUARE)
		{
			return MOVE_INVALID;
		}
		this->FirstMove = !this->FirstMove;
	}
	else // If it's not the pawn's first move
	{
		// If the pawn is moving more than one square forward, return 6
		if (parse != 1)
		{
			return MOVE_INVALID;
		}
	}
	return MOVE_VALID; // If the move is valid, return 0
}

// This function returns the value of FirstMove
bool Pawn::getFirstMove()
{
	return this->FirstMove;
}

// This function sets the value of FirstMove
void Pawn::setFirstMove(bool FirstPawnMove)
{
	this->FirstMove = FirstPawnMove;
}
