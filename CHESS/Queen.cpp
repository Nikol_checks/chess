#include "Queen.h"

/*
constructor of Queen.
input: name, type and position of Piece.
output: none.
*/
Queen::Queen(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
    // The constructor simply calls the constructor of the Piece class
}

Queen::~Queen()
{
    name = ' ';
    type = 0;
    xLocation = 0;
    yLocation = 0;
}

// This function checks if the queen can move to a given position
int Queen::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
    // Create a bishop and a rook with the same properties as the queen
    Bishop bishop(this->name, this->type, this->xLocation, this->yLocation);
    Rook rook(this->name, this->type, this->xLocation, this->yLocation);

    // The queen can move like a bishop or a rook
    // If the move is valid for the bishop or the rook, it is valid for the queen
    if (bishop.isLegitMove(xDestPosition, yDestPosition, board) == 0 || rook.isLegitMove(xDestPosition, yDestPosition, board) == 0)
    {
        return MOVE_VALID; // Return 0 to indicate a valid move
    }
    return MOVE_INVALID; // If the move is not valid for either the bishop or the rook, it is not valid for the queen
}


