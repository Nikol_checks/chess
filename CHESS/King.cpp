#include "King.h"
#define XY_COORDINATE 1

King::King(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation){}


King::~King()
{
    name = ' ';
    type = 0;
    xLocation = 0;
    yLocation = 0;
}

// This function checks if the king can move to a given position
int King::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
    // The king can only move one square in any direction
    // If the absolute difference in the x or y coordinates is not 1, the move is not valid
    if (!(std::abs(xDestPosition - this->xLocation) == XY_COORDINATE || std::abs(yDestPosition - this->yLocation) == XY_COORDINATE))
    {
        return MOVE_INVALID; // Return 6 to indicate an invalid move
    }
    return MOVE_VALID; // If the move is valid, return 0
}



