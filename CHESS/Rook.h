#pragma once
#include "Piece.h"
class Board; // Forward declaration


class Rook :public Piece
{
public:
	
	Rook(const char name, const int type, int xLocation, int yLocation);
	~Rook();
	//check if the the rook can reach the choosen block with his way of moving
	int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])override;

};