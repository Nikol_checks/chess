#include "Bishop.h"
Bishop::Bishop(const char name, const int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
}
Bishop::~Bishop()
{
	name = ' ';
	type = 0;
	xLocation = 0;
	yLocation = 0;
}

/*
this function returns if the rook can reach given position
input: position to check.
output: true or false
*/
int Bishop::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
	int indexY = 0,indexX=0,incOrdecY=0,i=0,incOrdecX=0;
	if (std::abs(this->yLocation-yDestPosition)!= std::abs(this->xLocation- xDestPosition))//check if the target block is an diagonal line
	{
		return MOVE_INVALID;
	}
	incOrdecX= xDestPosition < this->xLocation? -1 : 1;
	incOrdecY = yDestPosition < this->yLocation ? -1 : 1;
	//update the position for next check
	indexY = this->yLocation +incOrdecY;
	indexX = this->xLocation + incOrdecX;
	for (i = 0; i < std::abs(this->xLocation - xDestPosition)-1; i++)// a loop that check if their is a piece in the path of the target
	{
		if (board[indexY][indexX] != nullptr)
		{
			return MOVE_INVALID;
		}
		indexY += incOrdecY;
		indexX += incOrdecX;
	}
	return MOVE_VALID;
}
