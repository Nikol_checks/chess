#include "Knight.h"

#define KNIGHT_VALID_X_MOVE 2
#define KNIGHT_VALID_Y_MOVE 1

/*
constructor of Knight.
input: name, type and position of Piece.
output: none.
*/
Knight::Knight(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation) {}

Knight::~Knight()
{
	name = ' ';
	type = 0;
	xLocation = 0;
	yLocation = 0;
}

/*
this function returns if the Knight can reach given position
input: position to check.
output: true or false
*/

int Knight::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
	int xMove = abs(xDestPosition - this->xLocation);
	int yMove = abs(yDestPosition - this->yLocation);

	if ((xMove == KNIGHT_VALID_X_MOVE && yMove == KNIGHT_VALID_Y_MOVE) || (xMove == KNIGHT_VALID_Y_MOVE && yMove == KNIGHT_VALID_X_MOVE))
	{
		return MOVE_VALID; // valid move
	}
	return MOVE_INVALID; // invalid move
}

