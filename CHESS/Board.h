#pragma once
#include <map>
#include<iostream>
#include "King.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Piece.h"

#define MIN 0
#define SIZE_BOARD 8
#define BLACK 1
#define WHITE 0
#define X_PLACE 1
#define Y_PLACE 0

#define PIECE_NULL 2
#define WRONG_TURN 2
#define SAME_TYPE_PIECE 3
#define KING_CHECKED 4
#define PIECE_ALREADY_AT_DEST 7

class Board
{
private:
     Piece* board[SIZE_BOARD][SIZE_BOARD];//linking between the piece code and the board
    bool isWhiteTurn;
public:
    Board();
    void decrypted(std::string strLocation, int* location);
    std::string moveBoard(std::string messege);
    int eatPiece(int xPosition, int yPosition);
    int canMove(int xPosition, int yPosition, Piece* piece);//check uf the nove is legit by using the is legit move in each piece


    std::string move(Piece* piece, int xLoction, int yLoction);
    
    

    bool isKingChecked(int typeKing, int typeSoliders);//check if the king got checked
    
    void changeTurn();//whan the user make a valid move it will chenge for the other team to use their players

    int* getKingLocation(int type);// get the location for the king(work with white and black) used for checking check 
};
