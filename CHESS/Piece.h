#pragma once
#include <string>
#define MOVE_INVALID 6
#define MOVE_VALID 0
#define XY_BOARD_COORDINATE 8
/////////////////////////////////////////////////////////////////////
///    the abstratic code for all the functions for the pieces    ///
/////////////////////////////////////////////////////////////////////

using std::string;
using std::to_string;

class Piece
{
public:
	//c'tor
	Piece(char name, int type, int xLocation, int yLocation);
	
	//setters and getters
	void setName(const char  name);			//set the name for the piece 
	void setType(const int type);			//set the type for the piece
	int get_xLocation();					//get from the board the location of x of the piece
	int get_yLocation();					// get the y location of piece
	void set_yLocation(int yLocation);		//set the y location for the piece
	void set_xLocation(int xLocation);		//swt the x location of the piece

	char  getName() const;	//set the name by P p Q q B b...
	int getType() const;
	//other methods
	virtual int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])=0;		//abstratic function for all the pieces to check if the move is legit for the specific piece
	bool operator==(const Piece& other) const;

protected:
	char name;  
	int type;
	int xLocation;
	int yLocation;
};
