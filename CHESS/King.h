#pragma once
#include "Piece.h"
class Board; // Forward declaration


class King :public Piece
{
public:
    King(char name, int type, int xLocation, int yLocation);
    ~King();
    //check if the the king can reach the choosen block with his way of moving
    int isLegitMove(int xDestPosition, int yDestPosition,Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])override;



};



