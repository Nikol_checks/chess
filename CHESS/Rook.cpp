#include "Rook.h"

/*
constructor of Rook.
input: name, type and position of Piece.
output: none.
*/
Rook::Rook(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
}

Rook::~Rook()
{
	name = ' ';
	type = 0;
	xLocation = 0;
	yLocation = 0;
}

/*
this function returns if the rook can reach given position
input: position to check.
output: true or false
*/
// This function checks if the rook can move to a given position
int Rook::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[XY_BOARD_COORDINATE][XY_BOARD_COORDINATE])
{
	int index = 0;
	// The rook can only move in a straight line horizontally or vertically
	// If the rook is not moving in a straight line, return 6
	if (this->xLocation != xDestPosition && this->yLocation != yDestPosition)
	{
		return MOVE_INVALID;
	}
	// If the rook is moving horizontally
	if (this->xLocation != xDestPosition)
	{
		// Determine the direction of movement
		index = xDestPosition > this->xLocation ? this->xLocation + 1 : this->xLocation - 1;
		// Check all the squares on the way to the destination
		for (int j = 0; j < std::abs(xDestPosition - this->xLocation)-1; j++)
		{
			// If there is a piece on the square, return 6
			if (board[yLocation][index] != nullptr)
			{
				return MOVE_INVALID;
			}
			// Update the index for the next check
			index += xDestPosition > this->xLocation  ? 1 : -1;
		}
	}
	else // If the rook is moving vertically
	{
		// Determine the direction of movement
		index = yDestPosition > this->yLocation ? this->yLocation + 1 : this->yLocation - 1;
		// Check all the squares on the way to the destination
		for (int j = 0; j < std::abs(yDestPosition - this->yLocation)-1; j ++)
		{
			// If there is a piece on the square, return 6
			if (board[index][xLocation] != nullptr) 
			{
				return MOVE_INVALID;
			}
			// Update the index for the next check
			index += yDestPosition > this->yLocation? 1 : -1;
		}
	}
	return MOVE_VALID; // If the move is valid, return 0
}
